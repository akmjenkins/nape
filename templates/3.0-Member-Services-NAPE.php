<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
		
				<div class="overview-header">
					
					<div class="overview-content">
						
						<div class="hgroup article-head">
							<h1 class="title">Member Services</h1>
							<span class="subtitle">Information and Services for NAPE Members</span>
						</div><!-- .hgroup -->
						
						<div class="article-body">
						
							<p>
								NAPE works hard every day to ensure fair and equitable working conditions for its members across Newfoundland and Labrador. 
								We're committed to protecting the rights of our members, and offer a wide range of benefits and services to meet their needs.
							</p>
						
						</div><!-- .article-body -->
						
					</div><!-- .overview-content -->
					
					<div class="overview-aside">
						<img src="../assets/dist/images/temp/random-silhouette.jpg" alt="silhouette">
					</div><!-- .overview-aside -->
					
				</div><!-- .overview-header -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="overview-blocks">
			
				<div class="grid eqh overview-grid">
				
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Collective Agreements</h3>
								<span class="subtitle">Find your Collective Agreement</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Collective Bargaining Updates</h3>
								<span class="subtitle">Working together to create better workplaces and communities</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Contact your ERO</h3>
								<span class="subtitle">Get in touch with your NAPE Employee Relations Officer (ERO)</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Forms &amp; Applications</h3>
								<span class="subtitle">How it all Started</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Contests</h3>
								<span class="subtitle">Lorem ipsum dolor sit amet</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Your NAPE Local</h3>
								<span class="subtitle">Lorem ipsum dolor sit amet</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					
				</div><!-- .grid -->
			
			</div><!-- .overview-blocks -->
			
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>