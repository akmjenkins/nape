<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Contact Your ERO</h1>
					<span class="subtitle">Get in touch with your NAPE Employee Relations Officer (ERO)</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
						
						<div class="single-form-wrap">
							<form action="" class="single-form">
								<div class="fieldset">
									<input type="text" placeholder="Search by Name or your NAPE Local No.">
									<button type="submit" class="fa-search">&nbsp;</button>
								</div>
							</form>
							<span class="single-form-meta">
								Find Your ERO
							</span><!-- .form-meta -->
						</div><!-- .single-form-wrap -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>