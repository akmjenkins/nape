<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">

			<div class="hgroup article-head">
				<h1 class="title">Contact Us</h1>
				<span class="subtitle">Uniting over 25,000 workers across the province of Newfoundland &amp; Labrador.</span>
			</div><!-- .hgroup -->
		
			<div class="main-body">
				<div class="content">
				
					<div class="grid">
						<div class="col-3-5 col sm-col-1">
							<div class="item">
								<form action="/" class="body-form">
									<fieldset>
										
										<input type="text" name="name" placeholder="Your Name">
										<input type="email" name="email" placeholder="Your Email">
										
										<textarea name="message" cols="30" rows="10" placeholder="Your Message"></textarea>
										
										<button type="submit" class="button fill">Submit</button>
										
									</fieldset>
								</form>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2-5 col sm-col-1">
							<div class="item">

								<div class="mod-contact-us">
							
									<div class="swiper-wrapper">
									
										<div class="selector with-arrow">
											<select class="swiper-nav">
												<option>Head Office</option>
												<option>Central Office</option>
												<option>Western Office</option>
											</select>
											
											<span class="value">&nbsp;</span>
										</div><!-- .selector -->
									
										<div class="swiper" data-arrows="false">
											<div class="swipe-item">
											
												<div class="grid">
													<div class="col col-2 xs-col-1">									
														<address>
															330 Portual Cove Rd <br />
															P.O. Box 8100 <br />
															St. John's, NL, Canada A1B 3M9
														</address>
													</div><!-- .col -->
													<div class="col col-2 xs-col-1">
														
														<div class="rows">
														
															<div class="row">
																<span class="l">Phone:</span>
																<span class="r">(709) 754-0700</span>
															</div>
															
															<div class="row">
																<span class="l">TF:</span>
																<span class="r">1-800-563-4442</span>
															</div>
															
															<div class="row">
																<span class="l">Fax:</span>
																<span class="r">(709) 754-0726</span>
															</div>
															
														</div><!-- .rows -->
														
													</div><!-- .col -->
												</div><!-- .grid -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2690.4953991147536!2d-52.7296232!3d47.597056099999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca40370c2d14f%3A0x77d581c27c41f5ab!2s330+Portugal+Cove+Pl%2C+St.+John&#39;s%2C+NL+A1A+4Y5!5e0!3m2!1sen!2sca!4v1421697462238" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->
											
											<div class="swipe-item">
											
												<div class="grid">
													<div class="col col-2 xs-col-1">									
														<address>
															15 Hardy Avenue <br />
															P.O. Box 160 <br />
															Grand Falls-Windsor, NL, Canada A2A 2J4
														</address>
													</div><!-- .col -->
													<div class="col col-2 xs-col-1">
														
														<div class="rows">
														
															<div class="row">
																<span class="l">Phone:</span>
																<span class="r">(709) 489-6619</span>
															</div>
															
															<div class="row">
																<span class="l">TF:</span>
																<span class="r">1-800-563-1050</span>
															</div>
															
															<div class="row">
																<span class="l">Fax:</span>
																<span class="r">(709) 489-6657</span>
															</div>
															
														</div><!-- .rows -->
														
													</div><!-- .col -->
												</div><!-- .grid -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2620.09014304368!2d-55.6464523!3d48.951769399999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b773a445edca99f%3A0x5717c114449c6da!2s15+Hardy+Ave%2C+Grand+Falls-Windsor%2C+NL+A2A!5e0!3m2!1sen!2sca!4v1421698025860" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->
											
											<div class="swipe-item">
											
												<div class="grid">
													<div class="col col-2 xs-col-1">									
														<address>
															10 Main Street <br />
															P.O. Box 864 <br />
															Corner Brook, NL, Canada A2H 6H6
														</address>
													</div><!-- .col -->
													<div class="col col-2 xs-col-1">
														
														<div class="rows">
														
															<div class="row">
																<span class="l">Phone:</span>
																<span class="r">(709) 639-8483</span>
															</div>
															
															<div class="row">
																<span class="l">TF:</span>
																<span class="r">1-800-563-9343</span>
															</div>
															
															<div class="row">
																<span class="l">Fax:</span>
																<span class="r">(709) 639-1079</span>
															</div>
															
														</div><!-- .rows -->
														
													</div><!-- .col -->
												</div><!-- .grid -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2620.0770875220123!2d-57.9475502!3d48.952017999999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b7bb1fa35965863%3A0x72fd35dd4c40b17c!2s10+Main+St%2C+Corner+Brook%2C+NL+A2H!5e0!3m2!1sen!2sca!4v1421698056397" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->
											
											
											
											
										</div><!-- .swiper -->
									</div><!-- .swiper-wrapper -->

								</div><!-- .mod-contact-us -->
								
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
					
					
				</div><!-- .content -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>