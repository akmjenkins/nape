<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw full">
			
			<div class="main-body">
				<div class="content">
				
					<div class="paginated-items-container">
						<div class="paginated-items-header">
							
							<span class="title">Search Results for NAPE</span>
							<span class="date">Post Date</span>
							
						</div><!-- .paginated-items-header -->
						
						<div class="paginated-items">
						
							<a class="search-result" href="#">
							
								<div class="lazybg">
									<img src="../assets/dist/images/temp/news.jpg" alt="microphone">
								</div>
							
								<div class="search-content">
									<span>Assault on Social Worker and Increased Incidents of Violence Towards Workers Concerning for NAPE President</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->

								<time datetime="2014-01-01">3 days ago</time>
							</a><!-- .search-result -->
							
							<a class="search-result" href="#">
							
								<div class="lazybg">
									<img src="../assets/dist/images/temp/news.jpg" alt="microphone">
								</div>

								<div class="search-content">								
									<span>NAPE Activists to Gather on Province’s West Coast for Local Officer Training Seminar</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->

								<time datetime="2014-01-01">21 days ago</time>
							</a><!-- .search-result -->
							
							<a class="search-result" href="#">
							
								<div class="lazybg">
									<img src="../assets/dist/images/temp/news.jpg" alt="microphone">
								</div>
								
								<div class="search-content">
									<span>NAPE Responds to Announcement of New Courthouse Facility; Wants Input in Planning</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->

								<time datetime="2014-01-01">31 days ago</time>
							</a><!-- .search-result -->
							
							<a class="search-result" href="#">
							
								<div class="search-content">
									<span>A Labour of Love &mdash; Christian Children’s Fund of Canada</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->

								<time datetime="2014-01-01">44 days ago</time>
							</a><!-- .search-result -->
							
							<a class="search-result" href="#">
							
								<div class="lazybg">
									<img src="../assets/dist/images/temp/news.jpg" alt="microphone">
								</div>
								
								<div class="search-content">
									<span>UPCOMING SHOP STEWARD SEMINAR – REGION 5</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->

								<time datetime="2014-01-01">46 days ago</time>
							</a><!-- .search-result -->
							
							<a class="search-result" href="#">
							
								<div class="lazybg">
									<img src="../assets/dist/images/temp/news.jpg" alt="microphone">
								</div>
								
								<div class="search-content">
									<span>NAPE JOB POSTING – Two ERO Positions (NOW CLOSED)</span>
									<span class="button fill">Read More</span>
								</div><!-- .search-content -->
								
								<time datetime="2014-01-01">88 days ago</time>
							</a><!-- .search-result -->
							
						</div><!-- .paginated-items -->
						
						<div class="paginated-items-footer">
						
							<div class="arrow-controls">
								<!-- these can also be "a" tags -->
								<button class="prev">Prev</button>
								<button class="next">Next</button>
							</div><!-- .arrow-controls -->
							
							<div class="count">6 of 108</div>
						
						</div><!-- .search-result-footer -->
						
					</div><!-- .paginated-items-container -->
				
				</div><!-- .content -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>