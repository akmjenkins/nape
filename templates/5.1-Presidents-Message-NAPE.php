<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<div class="hgroup article-head">
				<h1 class="title">President's Message</h1>
				<span class="subtitle">Vivamus pulvinar tortor eget nibh gravida</span>
			</div><!-- .hgroup -->
				
			<div class="the-latest-header">
			
				<div class="news-update featured-news-update">
				
					<div class="news-update-head">
						<time datetime="2014-03-24" class="i blk">
							<span class="day">24</span> Mar
							<span class="year">2014</span>
						</time><!-- .i.blk -->
						
						<h3 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
					</div><!-- .news-update-head -->
					
					<div class="news-update-content">
					
						<p>
							Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidu
							nt sapien ullamcorper at. Quisque mollis neque ultrices orci varius rhoncus.
							Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
							Fusce nec facilisis lorem, id posuere mi.
						</p>
					
					</div><!-- .news-update-content -->
					
					<div class="news-update-actions">
						<a href="#" class="button fill">More</a>
						<a href="#" class="button fill share">Share</a>
						<a href="#" class="button fill tweet">Tweet</a>
					</div><!-- .news-update-acions -->
				
				</div><!-- .news-update -->

				<div class="the-latest-header-image video">
					<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="20:13"></div>
				</div><!-- .the-latest-header-image -->
				
			</div><!-- .the-latest-header -->

		</div><!-- .sw -->
	</section>
	
	
	<section>
		<div class="sw">
		
			<h3 class="section-title">Past Messages</h3>
			<hr />
			
			<div class="grid media-grid eqh fill">
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>