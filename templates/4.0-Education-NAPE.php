<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
		
				<div class="overview-header">
					
					<div class="overview-content">
						
						<div class="hgroup article-head">
							<h1 class="title">Education</h1>
							<span class="subtitle">Knowledge is power</span>
						</div><!-- .hgroup -->
						
						<div class="article-body">
						
							<p>
								Education is an integral and ongoing part of our union movement. It is about sharing information, analyzing situations, gaining new insight 
								and knowledge, developing our skills and organizing for action. Education of our members and activists is the backbone of a strong union. 
								NAPE is continually working to educate, inform, update and train members in an effort to build better workplace environments, better services 
								for the public, and better communities.
							</p>
						
						</div><!-- .article-body -->
						
					</div><!-- .overview-content -->
					
					<div class="overview-aside">
						<img src="../assets/dist/images/temp/random-silhouette.jpg" alt="silhouette">
					</div><!-- .overview-aside -->
					
				</div><!-- .overview-header -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="overview-blocks">
			
				<div class="grid eqh overview-grid">
				
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Shop Steward Training</h3>
								<span class="subtitle">The Backbone of the Union</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Women's Conference</h3>
								<span class="subtitle">Your NAPE Board Members and Executive</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Labour School</h3>
								<span class="subtitle">NAPE Staff Members</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Publications</h3>
								<span class="subtitle">How it all started</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Scholarships</h3>
								<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .overview-blocks -->
			
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>