<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Women's Conference</h1>
					<span class="subtitle">Sisters in Solidarity</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<div class="lazybg featured-img">
								<img src="../assets/dist/images/temp/featured-img-2.jpg" alt="featured-img-2">
							</div><!-- .lazybg -->
							
							<p>
								Each year NAPE holds a Women’s Conference to discuss issues pertinent to women in the context of the labour movement. 
								The conference normally consists of some combination of guest speakers, panelists, workshops, and seminars.
							</p>
 
							<p>
								The goal of the Women’s Conference is to build women’s political capacity and activism in the union, the community and beyond.
							</p>
 
							<p>
								The conference is organized by NAPE’s Women’s Committee.
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<h3 class="section-title">2014 Women's Conference</h3>
			<hr />
			
			<div class="grid collapse-950">
				<div class="col col-3-5">
					<div class="item">
						
						<div class="swiper-wrapper conference-swiper">
							<div class="flex-controls">
							
								<div>
									<time class="i dow blk">
										<span class="dow">Fri</span>
										<div>
											<span class="day">24</span> 
											Mar
											<span class="year">2014</span>
										</div>
									</time>
									<time class="i dow blk">
										<span class="dow">Sat</span>
										<div>
											<span class="day">26</span> 
											Mar
											<span class="year">2014</span>
										</div>
									</time>
									<time class="i dow blk">
										<span class="dow">Sun</span>
										<div>
											<span class="day">26</span> 
											Mar
											<span class="year">2014</span>
										</div>
									</time>
								</div>
								
								<div class="arrow-controls">
									<!-- these can also be "a" tags -->
									<button class="prev">Prev</button>
									<button class="next">Next</button>
								</div><!-- .arrow-controls -->
								
							</div><!-- .flex-controls -->
						
							<div class="swiper" data-responsive='{"breakpoint":600,"settings":{"arrows":true,"slidesToShow":1}}' data-center-padding="20" data-slides-to-show="2" data-next-arrow=".conference-swiper .arrow-controls .next" data-prev-arrow=".conference-swiper .arrow-controls .prev">
							
								<div class="swipe-item">
									<a href="#" class="item grid-item-with-tag clear">
										<span class="item-tag tag-event">&nbsp;</span>
										
										<time datetime="2014-03-24" class="i">
											<span class="day">24</span> Mar
										</time>
										
										<p>												
											4:00-5:30 Registration-Main lobby of hotel <br />
											<br />
											5:30-6:30 Supper <br />
											<br />
											7:00 Welcome/Opening Remarks/Introductions <br />
											<br />
											President and Secretary/Treasurer’s Address <br />
											<br />
											8:30 Wine & Hors d’oeuvres
										</p>
										
										<span class="read-more-tag">Read More</span>
									</a><!-- .item -->
								</div><!-- .swipe-item -->
								<div class="swipe-item">
									<a href="#" class="item grid-item-with-tag clear">
										<span class="item-tag tag-event">&nbsp;</span>
										
										<time datetime="2014-03-24" class="i">
											<span class="day">25</span> Mar
										</time>
										
										<p>												
											Saturday, June 1 (7:30-8:30 Breakfast) <br />
											<br />
											9:00-10:30 Concurrent Sessions <br />
											<br />
											Self-Defense For Women 
											(Robinson/Carrick Rooms, 1st Floor) <br />
											<br />
											Work Life/Balance <br />
											(Ocean View Room, 2nd Floor)
										</p>
										
										<span class="read-more-tag">Read More</span>
									</a><!-- .item -->								
								</div><!-- .swipe-item -->
								
								<div class="swipe-item">
									<a href="#" class="item grid-item-with-tag clear">
										<span class="item-tag tag-event">&nbsp;</span>
										
										<time datetime="2014-03-24" class="i">
											<span class="day">26</span> Mar
										</time>
										
										<p>												
											4:00-5:30 Registration-Main lobby of hotel <br />
											<br />
											5:30-6:30 Supper <br />
											<br />
											7:00 Welcome/Opening Remarks/Introductions <br />
											<br />
											President and Secretary/Treasurer’s Address <br />
											<br />
											8:30 Wine & Hors d’oeuvres
										</p>
										
										<span class="read-more-tag">Read More</span>
									</a><!-- .item -->
								</div><!-- .swipe-item -->
							
							</div><!-- .swiper -->
							
						</div><!-- .swiper-wrapper -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2-5">
					<div class="item">
					
						<div class="hgroup">
							<h4 class="title">Registration</h4>
							<span class="subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
						</div><!-- .hgroup -->
						
						<p>
							<strong>LOCATION:</strong>
							Terra Nova Golf Resort, Port Blandford <br />
							<br />
							
							<strong>DATE:</strong>
							Dec 8 - Dec 10, 2014 <br />
							
							<strong>CONTACT:</strong>
							754-0700 or toll free 1-800-563-4442
						</p>
						
						<br />
						
						<a href="#" class="inline">bevans@nape.nf.ca</a> <br />
						<a href="#" class="inline">ebursey@nape.nf.ca</a> <br />
						<a href="#" class="inline">rconnors@nape.nf.ca</a> <br />
						
						<br />
						
						<a href="#" class="button fill">Download Application</a>
						
						<br />
						<br />
						
						<p>
							The deadline for the application is Nov. 9, 2014.
						</p>
						
					</div><!-- .item -->
				</div><!-- .col.col-2-5 -->
			</div><!-- .grid -->
			
			
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>