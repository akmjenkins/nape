<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<div class="hgroup article-head">
				<h1 class="title">News</h1>
				<span class="subtitle">Vivamus pulvinar tortor eget nibh gravida</span>
			</div><!-- .hgroup -->
				
			<div class="the-latest-header">
			
				<div class="news-update featured-news-update">
				
					<div class="news-update-head">
						<time datetime="2014-03-24" class="i blk">
							<span class="day">24</span> Mar
							<span class="year">2014</span>
						</time><!-- .i.blk -->
						
						<h3 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
					</div><!-- .news-update-head -->
					
					<div class="news-update-content">
					
						<p>
							Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidu
							nt sapien ullamcorper at. Quisque mollis neque ultrices orci varius rhoncus.
							Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
							Fusce nec facilisis lorem, id posuere mi.
						</p>
					
					</div><!-- .news-update-content -->
					
					<div class="news-update-actions">
						<a href="#" class="button fill">More</a>
						<a href="#" class="button fill share">Share</a>
						<a href="#" class="button fill tweet">Tweet</a>
					</div><!-- .news-update-acions -->
				
				</div><!-- .news-update -->

				<div class="the-latest-header-image video">
					<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="20:13"></div>
				</div><!-- .the-latest-header-image -->
				
			</div><!-- .the-latest-header -->

		</div><!-- .sw -->
	</section>
	
	
	<section>
		<div class="sw">
		
			<h3 class="section-title">Other News</h3>
			<hr />
			
			<div class="filter-section">
				
				<div class="filter-bar">
					
					<div class="filter-bar-left">
					
						<div class="selector with-arrow">
							<select>
								<option value="">Last 10 Days</option>
								<option value="">Last 20 Days</option>
								<option value="">Last 30 Days</option>
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->
						
					</div><!-- .filter-bar-left -->
				
					<div class="filter-bar-meta">
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search updates...">
								<button class="fa-search">&nbsp;</button>
							</fieldset>
						</form>
					
					</div><!-- .filter-bar-meta -->
						
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
				
					<div class="grid eqh collapse-800 grid-tagged-items the-latest-grid">
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news.jpg"></div>
								</div><!-- .ar -->
							
								<div class="the-latest-grid-content">					
									<span class="item-tag tag-news-release">News Release</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">24</span> Mar
									</time>
									
									<p>
										NAPE Says Code Red Alert Increases Due to Shortage of Ambulances and Paramedic Staff.
									</p>
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news-2.jpg"></div>
								</div><!-- .ar -->
						
								<div class="the-latest-grid-content">
									<span class="item-tag tag-alert">Alert</span>
									
									<time datetime="2014-03-03" class="i">
										<span class="day">03</span> Mar
									</time>
									
									<p>
										Eastern Health Paramedics to Hold Demonstration at Confederation Building Tomorrow.
									</p>
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news-3.jpg"></div>
								</div><!-- .ar -->
							
								<div class="the-latest-grid-content">
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-12" class="i">
										<span class="day">12</span> Mar
									</time>
									
									<p>
										Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
									</p>
									
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news.jpg"></div>
								</div><!-- .ar -->
							
								<div class="the-latest-grid-content">					
									<span class="item-tag tag-news-release">News Release</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">24</span> Mar
									</time>
									
									<p>
										NAPE Says Code Red Alert Increases Due to Shortage of Ambulances and Paramedic Staff.
									</p>
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news-2.jpg"></div>
								</div><!-- .ar -->
						
								<div class="the-latest-grid-content">
									<span class="item-tag tag-alert">Alert</span>
									
									<time datetime="2014-03-03" class="i">
										<span class="day">03</span> Mar
									</time>
									
									<p>
										Eastern Health Paramedics to Hold Demonstration at Confederation Building Tomorrow.
									</p>
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3">
							<a class="item dark-bg grid-item-with-tag" href="#">
							
								<div class="the-latest-grid-img">
									<div class="lazybg" data-src="../assets/dist/images/temp/news-3.jpg"></div>
								</div><!-- .ar -->
							
								<div class="the-latest-grid-content">
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-12" class="i">
										<span class="day">12</span> Mar
									</time>
									
									<p>
										Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
									</p>
									
									
									<span class="button">Read More</span>
								</div><!-- .the-latest-grid-content -->
								
							</a><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				
				</div><!-- .filter-content -->
				
			</div><!-- .filter-section -->

			
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>