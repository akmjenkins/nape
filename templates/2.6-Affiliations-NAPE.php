<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Affiliations</h1>
					<span class="subtitle">Strength in Numbers</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<p>
								NAPE is an active affiliate of the Newfoundland and Labrador Federation of Labour (NLFL), the National Union of Public and 
								General Employees (NUPGE), and the Canadian Labour Congress (CLC).
							</p>
						
						</div><!-- .article-body -->
						
						<div class="grid eqh affiliates-grid">
						
							<div class="col col-3">
								<a class="item" href="#">
									
									<div class="vcenter-content">
									
										<div class="lazybg ib">
											<img src="../assets/dist/images/temp/affiliates/national-union.png" alt="NUPGE">
										</div><!-- .lazybg -->
										
										<span>
											NUPGE (National Union of Public and General Employees)
										</span>
										
									</div><!-- .vcenter-content -->
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col col-3">
								<a class="item" href="#">
									
									<div class="vcenter-content">
									
										<div class="lazybg ib">
											<img src="../assets/dist/images/temp/affiliates/nlfl.png" alt="NLFL">
										</div><!-- .lazybg -->
										
										<span>
											NLFL (Newfoundland and Labrador Federation of Labour)
										</span>
										
									</div><!-- .vcenter-content -->
									
								</a><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-3">
								<a class="item" href="#">
									
									<div class="vcenter-content">
									
										<div class="lazybg ib">
											<img src="../assets/dist/images/temp/affiliates/cdn-labour-congress.png" alt="CLC">
										</div><!-- .lazybg -->
										
										<span>
											CLC (Canadian Labour Congress)
										</span>
										
									</div><!-- .vcenter-content -->
									
								</a><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="overview-blocks">
			
				<div class="grid eqh overview-grid">
				
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">President's Bio</h3>
								<span class="subtitle">Carol Furlong</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Executive and Board</h3>
								<span class="subtitle">Your NAPE board members and executive and a longer subtitle</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Staff or a longer title variation</h3>
								<span class="subtitle">NAPE Staff Members</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">History of Nape</h3>
								<span class="subtitle">How it all started</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Constitution</h3>
								<span class="subtitle">The Constitution of Nape</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">NAPE Anthem</h3>
								<span class="subtitle">The official anthem of our union</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Affiliations</h3>
								<span class="subtitle">Strength in Numbers</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Quick Links</h3>
								<span class="subtitle">Helpful websites for NAPE Members</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					
				</div><!-- .grid -->
			
			</div><!-- .overview-blocks -->
			
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>