<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
			
			<div class="hgroup article-head">
				<h1 class="title">Collective Bargaining Updates</h1>
				<span class="subtitle">Working together to create better workplaces and communities</span>
			</div><!-- .hgroup -->
		
			<div class="main-body">
				<div class="content">

					<div class="news-update featured-news-update bordered-news-update">
					
						<div class="news-update-head">
							<time datetime="2014-03-24" class="i blk">
								<span class="day">24</span> Mar
								<span class="year">2014</span>
							</time><!-- .i.blk -->
							
							<h3 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
						</div><!-- .news-update-head -->
						
						<div class="news-update-content">
						
							<p>
								Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidu
								nt sapien ullamcorper at. Quisque mollis neque ultrices orci varius rhoncus.
								Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
								Fusce nec facilisis lorem, id posuere mi.
							</p>
						
						</div><!-- .news-update-content -->
						
						<div class="news-update-actions">
							<a href="#" class="button fill">More</a>
							<a href="#" class="button fill share">Share</a>
							<a href="#" class="button fill tweet">Tweet</a>
						</div><!-- .news-update-acions -->
					
					</div><!-- .news-update -->
				
				
					<h3 class="section-title">More Updates</h3>
					<hr />
					
					<div class="filter-section">
						
						<div class="filter-bar">
							
							<div class="filter-bar-left">
							
								<div class="selector with-arrow">
									<select>
										<option value="">The Latest</option>
										<option value="">Most Popular</option>
									</select>
									<span class="value">&nbsp;</span>
								</div><!-- .selector -->
								
							</div><!-- .filter-bar-left -->
						
							<div class="filter-bar-meta">
							
								<form action="/" method="post" class="search-form single-form">
									<fieldset>
										<input type="text" name="s" placeholder="Search updates...">
										<button class="fa-search">&nbsp;</button>
									</fieldset>
								</form>
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
							</div><!-- .filter-bar-meta -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">
						
							<div class="grid eqh news-update-grid fill">
								<div class="col col-2">
									<div class="news-update item">
									
										<div class="news-update-head">
											<time datetime="2014-03-24" class="i blk">
												<span class="day">24</span> Mar
												<span class="year">2014</span>
											</time><!-- .i.blk -->
											
											<h4 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h4>
										</div><!-- .news-update-head -->
										
										<div class="news-update-content">
										
											<p>
												Praesent consectetur augue leo, quis ultricies orci porta
												ut. Crasvehicula nisl ligula, ut tincidunt sapien
												ullamcorper at. Quisque mollis neque ultrices orci varius
												rhoncus.
											</p>
										
										</div><!-- .news-update-content -->
										
										<div class="news-update-actions">
											<a href="#" class="button fill">More</a>
											<a href="#" class="button fill share">Share</a>
											<a href="#" class="button fill tweet">Tweet</a>
										</div><!-- .news-update-acions -->
									
									</div><!-- .news-update -->
								</div><!-- .col -->
								<div class="col col-2">
									<div class="news-update item">
									
										<div class="news-update-head">
											<time datetime="2014-03-24" class="i blk">
												<span class="day">24</span> Mar
												<span class="year">2014</span>
											</time><!-- .i.blk -->
											
											<h4 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h4>
										</div><!-- .news-update-head -->
										
										<div class="news-update-content">
										
											<p>
												Praesent consectetur augue leo, quis ultricies orci porta
												ut. Crasvehicula nisl ligula, ut tincidunt sapien
											</p>
										
										</div><!-- .news-update-content -->
										
										<div class="news-update-actions">
											<a href="#" class="button fill">More</a>
											<a href="#" class="button fill share">Share</a>
											<a href="#" class="button fill tweet">Tweet</a>
										</div><!-- .news-update-acions -->
									
									</div><!-- .news-update -->
								</div><!-- .col -->
								<div class="col col-2">
									<div class="news-update item">
									
										<div class="news-update-head">
											<time datetime="2014-03-24" class="i blk">
												<span class="day">24</span> Mar
												<span class="year">2014</span>
											</time><!-- .i.blk -->
											
											<h4 class="title">Fusce nec Nibh</h4>
										</div><!-- .news-update-head -->
										
										<div class="news-update-content">
										
											<p>
												Praesent consectetur augue leo, quis ultricies orci porta
												ut. Crasvehicula nisl ligula, ut tincidunt sapien
												ullamcorper at. Quisque mollis neque ultrices orci varius
												rhoncus. Crasvehicula nisl ligula, ut tincidunt sapien
												ullamcorper at. Quisque mollis neque ultrices orci varius
												rhoncus.
											</p>
										
										</div><!-- .news-update-content -->
										
										<div class="news-update-actions">
											<a href="#" class="button fill">More</a>
											<a href="#" class="button fill share">Share</a>
											<a href="#" class="button fill tweet">Tweet</a>
										</div><!-- .news-update-acions -->
									
									</div><!-- .news-update -->
								</div><!-- .col -->
								<div class="col col-2">
									<div class="news-update item">
									
										<div class="news-update-head">
											<time datetime="2014-03-24" class="i blk">
												<span class="day">24</span> Mar
												<span class="year">2014</span>
											</time><!-- .i.blk -->
											
											<h4 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h4>
										</div><!-- .news-update-head -->
										
										<div class="news-update-content">
										
											<p>
												Praesent consectetur augue leo, quis ultricies orci porta
												ut. Crasvehicula nisl ligula, ut tincidunt sapien
												ullamcorper at. Quisque mollis neque ultrices orci varius
												rhoncus.
											</p>
										
										</div><!-- .news-update-content -->
										
										<div class="news-update-actions">
											<a href="#" class="button fill">More</a>
											<a href="#" class="button fill share">Share</a>
											<a href="#" class="button fill tweet">Tweet</a>
										</div><!-- .news-update-acions -->
									
									</div><!-- .news-update -->
								</div><!-- .col -->
								
							</div><!-- .grid -->
						
						</div><!-- .filter-content -->
						
					</div><!-- .filter-section -->
				
				</div><!-- .content -->
				<aside class="sidebar">
				
					<div class="mod">
						<?php include('inc/i-mod-in-this-section.php'); ?>
					</div><!-- .mod -->
					
					<div class="mod">
						<?php include('inc/i-mod-the-latest.php'); ?>
					</div><!-- .mod -->
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>