			<div class="inline-search-results">
			
				<div class="grid">
					<div class="col col-3 sm-col-1">
						<div class="item">
						
							<div class="img-wrap no-xs lazybg ib">
								<img src="../assets/dist/images/temp/news.jpg">
							</div>
							
							<div class="hgroup">
								<h4 class="title">Headline</h4>
								<span class="subtitle">Sub-headline</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
							</p>
							
							<a href="#" class="button fill">Read More</a>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 sm-col-1">
						<div class="item">
						
							<div class="img-wrap no-xs lazybg ib">
								<img src="../assets/dist/images/temp/news-2.jpg">
							</div>
							
							<div class="hgroup">
								<h4 class="title">Headline</h4>
								<span class="subtitle">Sub-headline</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
							</p>
							
							<a href="#" class="button fill">Read More</a>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 sm-col-1">
						<div class="item">
						
							<div class="img-wrap no-xs lazybg ib">
								<img src="../assets/dist/images/temp/news-3.jpg">
							</div>
							
							<div class="hgroup">
								<h4 class="title">Headline</h4>
								<span class="subtitle">Sub-headline</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
							</p>
							
							<a href="#" class="button fill">Read More</a>
							
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			
			</div><!-- .inline-search-results -->
