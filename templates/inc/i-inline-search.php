			<div class="inline-search">
			
				<div class="inline-search-form">
					
					<div class="hgroup">
						<h3 class="title">What are you looking for?</h3>
						<span class="subtitle">Find your Collective Agreement, ERO, news, updates, upcoming events, and more here.</span>
					</div><!-- .hgroup -->
					
					<form action="/" class="single-form">
						<span class="count">15</span>
						<div class="fieldset">
							<input type="text" name="s" placeholder="Type your question, keyword, or phrase">
							<button type="submit" class="fa-search">&nbsp;</button>
						</div><!-- .fieldset -->
					</form>
					
				</div><!-- .inline-search-form -->
			
			</div><!-- .inline-search -->
