<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<a href="#" class="hero-bg-link"></a>
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="content">					
						<span class="title">Information on Public Service Pension Plan</span>
						<span class="subtitle">Lorem ipsum dipsum mipsum ahea dokieo</span>
					</div><!-- .content -->
					
					<a href="#" class="button">Read More</a>
					
				</div>
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
		<div class="fader-item" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<a href="#" class="hero-bg-link"></a>
			<div class="hero-caption">
				<div class="sw">
					
					<div class="content">
						<span class="title">Lorem ipsum dipsum mipsum ahea dokieo</span>
						<span class="subtitle">Information on Public Service Pension Plan</span>
					</div><!-- .content -->
					
					<a href="#" class="button">Read More</a>
					
				</div>
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
	<div class="fader-control-wrap sw">
		<div class="arrow-controls fader-controls"></div>
	</div><!-- .fader-control-wrap -->
	
</div><!-- .hero -->