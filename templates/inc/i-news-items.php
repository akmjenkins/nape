			<div class="news-items">
			
				<div class="grid collapse-1000">
				
					<div class="col-3 col">
						<div class="item">
							
							<h2>President's Message</h2>
							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-03-24">
										<span class="day">24</span>
										Mar
									</time><!-- .i -->
								
									<div class="h5-style">
										President's Video Message Tentative Agreement Reached
									</div>
								
								</div><!-- .pmessage-title -->
								
								<div class="article-body">
								
									<p>
										According to NAPE President Carol Furlong, the tentative agreement is based on the template...
									</p>
								
								</div><!-- .article-body -->
							
							</div><!-- .news-item -->

							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-06-07">
										<span class="day">07</span>
										Jun
									</time><!-- .i -->
								
									<div class="h5-style">
										President's Video Message Tentative Agreement Reached
									</div>
								
								</div><!-- .news-item-title -->
								
								<a href="#" class="button fill">Read Transcript</a>
								<a href="#" class="button fill">Past Messages</a>
							
							</div><!-- .news-item -->
							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-04-29">
										<span class="day">29</span>
										Apr
									</time><!-- .i -->
								
									<div class="h5-style">
										Have Province? Have People?
									</div>
								
								</div><!-- .news-item-title -->
								
								<a href="#" class="button fill">Read Transcript</a>
								<a href="#" class="button fill">Past Messages</a>
							
							</div><!-- .news-item -->
							
							<div class="news-items-controls">
							
								<div class="arrow-controls">
								
									<!-- these can also be "a" tags -->
									<button class="prev">Prev</button>
									<button class="next">Next</button>
									
								</div><!-- .controls -->
								
								<a href="#" class="button fill">View All</a>
							</div><!-- .news-items-controls -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-2-3 col">
						<div class="item">
							
							<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="20:13"></div>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .news-items -->
