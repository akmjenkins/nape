			<footer class="dark-bg">
				<div class="sw">
				
					<div class="footer-blocks">
					
						<div class="footer-block footer-contact-block">
						
							<h5>Newfounland &amp; Labrador Association of Public and Private Employees</h5>
								
							<address>
								330 Portugal Cove Place <br />
								P.O. Box 8100 <br />
								St. John's, NL <br />
								Canada, A1B 3M9
							</address>
							
							<div class="rows">
								<div class="row">
									<span class="l">P:</span>
									<span class="r">1 (709) 754-0700</span>
								</div>
								<div class="row">
									<span class="l">TF:</span>
									<span class="r">1 (800) 563-4442</span>
								</div>
								<div class="row">
									<span class="l">F:</span>
									<span class="r">1 (709) 754-0726</span>
								</div>
							</div><!-- .rows -->
							
							<a href="#" class="button">Contact Us</a>
						
						</div><!-- .footer-block -->
						
						<div class="footer-block footer-nav-block">
						
							<ul>
								<li><a href="#">About NAPE</a></li>
								<li><a href="#">Member Area</a></li>
								<li><a href="#">Member Services</a></li>
								<li><a href="#">Agreements</a></li>
								<li><a href="#">Education</a></li>
								<li><a href="#">Accessibility</a></li>
								<li><a href="#">The Latest</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
							
						</div><!-- .footer-block -->
						
						<div class="footer-block footer-links-block">
						
							<ul>
								<li><a href="#">Government of NL</a></li>
								<li><a href="#">Public Service Commission</a></li>
								<li><a href="#">WHSCC</a></li>
							</ul>
						
							<a href="#" class="button">View Links</a>
						
						
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">NAPE</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Sitemap</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<form action="/" method="get" class="global-search-form">
			<div class="fieldset">
				<input type="search" name="s" placeholder="Search NAPE...">
				<span class="close t-fa-abs fa-close toggle-search">Close</span>
			</div>
		</form><!-- .single-form -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/nape',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>