<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<button class="t-fa-abs fa-search toggle-search mobile-toggle-search">
	Search
</button>

<div class="nav">
	<div class="sw full">
		
		<nav>
			<ul>
				<li>
					<a href="#">About Nape</a>
					<div>
						<ul>
							<li><a href="#">President's Bio</a></li>
							<li><a href="#">Executive and Board</a></li>
							<li><a href="#">Staff</a></li>
							<li><a href="#">History of NAPE</a></li>
							<li><a href="#">Constitution</a></li>
							<li><a href="#">NAPE Anthem</a></li>
							<li><a href="#">Affiliations</a></li>
							<li><a href="#">Quick Links</a></li>
						</ul>
					</div>
				</li>
				<li>
					<a href="#">Member Services</a>
				</li>
				<li>
					<a href="#">Education</a>
				</li>
				<li>
					<a href="#">The Latest</a>
				</li>
				<li>
					<a href="#">Contact Us</a>
				</li>
			</ul>
			
			
			<button class="t-fa-abs fa-search toggle-search">Search</button>
			
		</nav>
	
		<div class="nav-top">
		
			<a href="#">Home</a>
			<a href="#">Accessibility</a>
			<a href="#">Member Area</a>
			<a href="#">Agreements</a>
			
		</div><!-- .nav-top -->
	</div><!-- .sw -->
</div><!-- .nav -->