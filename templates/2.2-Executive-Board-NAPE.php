<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Executive and Board</h1>
					<span class="subtitle">Your NAPE board members and executive</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
						
						<div class="acc clean">
						
							<div class="acc-item">
								<div class="acc-item-handle">
									Executive
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
								
									<div class="grid member-grid">
									
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Carol Furlong</span>
													<span class="member-title">President</span>
													
													<span class="member-info phone">709-754-0700</span>
													<a href="#" class="member-info email">cfurlong@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bert Blundon</span>
													<span class="member-title">Secretary/Treasurer</span>
													
													<span class="member-info phone">709-570-2466</span>
													<a href="#" class="member-info email">bblundon@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Joanne Miles</span>
													<span class="member-title">Western Vice President</span>
													
													<span class="member-info phone">709-647-3381</span>
													<a href="#" class="member-info email">jrjkmiles@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bernadine Power</span>
													<span class="member-title">Central Vice President</span>
													
													<span class="member-info phone">709-489-7958</span>
													<span class="member-info phone">709-292-2242</span>
													<a href="#" class="member-info email">bpower@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">James Lacey</span>
													<span class="member-title">Eastern Vice President</span>
													
													<span class="member-info phone">709-777-6066</span>
													<span class="member-info phone">709-682-9019</span>
													<a href="#" class="member-info email">jklacey@hotmail.com</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Arlene Sedlicka</span>
													<span class="member-title">General Vice President</span>
													
													<span class="member-info phone">709-778-0480</span>
													<a href="#" class="member-info email">asedlickas@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Board Members
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
								
									<div class="grid member-grid">
									
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Carol Furlong</span>
													<span class="member-title">President</span>
													
													<span class="member-info phone">709-754-0700</span>
													<a href="#" class="member-info email">cfurlong@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bert Blundon</span>
													<span class="member-title">Secretary/Treasurer</span>
													
													<span class="member-info phone">709-570-2466</span>
													<a href="#" class="member-info email">bblundon@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Joanne Miles</span>
													<span class="member-title">Western Vice President</span>
													
													<span class="member-info phone">709-647-3381</span>
													<a href="#" class="member-info email">jrjkmiles@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bernadine Power</span>
													<span class="member-title">Central Vice President</span>
													
													<span class="member-info phone">709-489-7958</span>
													<span class="member-info phone">709-292-2242</span>
													<a href="#" class="member-info email">bpower@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">James Lacey</span>
													<span class="member-title">Eastern Vice President</span>
													
													<span class="member-info phone">709-777-6066</span>
													<span class="member-info phone">709-682-9019</span>
													<a href="#" class="member-info email">jklacey@hotmail.com</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Arlene Sedlicka</span>
													<span class="member-title">General Vice President</span>
													
													<span class="member-info phone">709-778-0480</span>
													<a href="#" class="member-info email">asedlickas@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Area Board Members
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
								
									<div class="grid member-grid">
									
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Carol Furlong</span>
													<span class="member-title">President</span>
													
													<span class="member-info phone">709-754-0700</span>
													<a href="#" class="member-info email">cfurlong@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bert Blundon</span>
													<span class="member-title">Secretary/Treasurer</span>
													
													<span class="member-info phone">709-570-2466</span>
													<a href="#" class="member-info email">bblundon@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Joanne Miles</span>
													<span class="member-title">Western Vice President</span>
													
													<span class="member-info phone">709-647-3381</span>
													<a href="#" class="member-info email">jrjkmiles@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bernadine Power</span>
													<span class="member-title">Central Vice President</span>
													
													<span class="member-info phone">709-489-7958</span>
													<span class="member-info phone">709-292-2242</span>
													<a href="#" class="member-info email">bpower@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">James Lacey</span>
													<span class="member-title">Eastern Vice President</span>
													
													<span class="member-info phone">709-777-6066</span>
													<span class="member-info phone">709-682-9019</span>
													<a href="#" class="member-info email">jklacey@hotmail.com</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Arlene Sedlicka</span>
													<span class="member-title">General Vice President</span>
													
													<span class="member-info phone">709-778-0480</span>
													<a href="#" class="member-info email">asedlickas@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Region Board Members
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
								
									<div class="grid member-grid">
									
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Carol Furlong</span>
													<span class="member-title">President</span>
													
													<span class="member-info phone">709-754-0700</span>
													<a href="#" class="member-info email">cfurlong@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bert Blundon</span>
													<span class="member-title">Secretary/Treasurer</span>
													
													<span class="member-info phone">709-570-2466</span>
													<a href="#" class="member-info email">bblundon@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Joanne Miles</span>
													<span class="member-title">Western Vice President</span>
													
													<span class="member-info phone">709-647-3381</span>
													<a href="#" class="member-info email">jrjkmiles@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bernadine Power</span>
													<span class="member-title">Central Vice President</span>
													
													<span class="member-info phone">709-489-7958</span>
													<span class="member-info phone">709-292-2242</span>
													<a href="#" class="member-info email">bpower@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">James Lacey</span>
													<span class="member-title">Eastern Vice President</span>
													
													<span class="member-info phone">709-777-6066</span>
													<span class="member-info phone">709-682-9019</span>
													<a href="#" class="member-info email">jklacey@hotmail.com</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Arlene Sedlicka</span>
													<span class="member-title">General Vice President</span>
													
													<span class="member-info phone">709-778-0480</span>
													<a href="#" class="member-info email">asedlickas@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Component Board Members
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
								
									<div class="grid member-grid">
									
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Carol Furlong</span>
													<span class="member-title">President</span>
													
													<span class="member-info phone">709-754-0700</span>
													<a href="#" class="member-info email">cfurlong@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bert Blundon</span>
													<span class="member-title">Secretary/Treasurer</span>
													
													<span class="member-info phone">709-570-2466</span>
													<a href="#" class="member-info email">bblundon@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Joanne Miles</span>
													<span class="member-title">Western Vice President</span>
													
													<span class="member-info phone">709-647-3381</span>
													<a href="#" class="member-info email">jrjkmiles@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Bernadine Power</span>
													<span class="member-title">Central Vice President</span>
													
													<span class="member-info phone">709-489-7958</span>
													<span class="member-info phone">709-292-2242</span>
													<a href="#" class="member-info email">bpower@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">James Lacey</span>
													<span class="member-title">Eastern Vice President</span>
													
													<span class="member-info phone">709-777-6066</span>
													<span class="member-info phone">709-682-9019</span>
													<a href="#" class="member-info email">jklacey@hotmail.com</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 member-col">
											<div class="item">
												
												<div class="member">
													
													<span class="member-name">Arlene Sedlicka</span>
													<span class="member-title">General Vice President</span>
													
													<span class="member-info phone">709-778-0480</span>
													<a href="#" class="member-info email">asedlickas@nape.nf.ca</a>
													
												</div><!-- .member -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
						</div><!-- .acc -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							
							<?php include('inc/i-mod-the-latest.php'); ?>
							
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>