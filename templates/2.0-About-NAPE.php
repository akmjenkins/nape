<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
		
				<div class="overview-header">
					
					<div class="overview-content">
						
						<div class="hgroup article-head">
							<h1 class="title">About NAPE</h1>
							<span class="subtitle">Uniting over 25,000 workers across the province of Newfoundland &amp; Labrador.</span>
						</div><!-- .hgroup -->
						
						<div class="article-body">
						
							<p>
								The Newfoundland and Labrador Association of Public and Private Employees (NAPE) is the largest union in the province and is the recognized bargaining agent for over 
								25,000 public and private sector employees.
							</p>
		 
							<p>
								Our members work in the civil service, at school boards, universities, colleges, hospitals, liquor stores, correctional facilities, Arts and Culture Centres, and 
								municipalities; they also work in homecare, transportation, air services, and private sector, as well as workers compensation, and other establishments across the province.
							</p>
						
						</div><!-- .article-body -->
						
					</div><!-- .overview-content -->
					
					<div class="overview-aside">
						<img src="../assets/dist/images/nape-logo.svg" alt="NAPE Logo">
					</div><!-- .overview-aside -->
					
				</div><!-- .overview-header -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="overview-blocks">
			
				<div class="grid eqh overview-grid">
				
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">President's Bio</h3>
								<span class="subtitle">Carol Furlong</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Executive and Board</h3>
								<span class="subtitle">Your NAPE board members and executive and a longer subtitle</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Staff or a longer title variation</h3>
								<span class="subtitle">NAPE Staff Members</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">History of Nape</h3>
								<span class="subtitle">How it all started</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Constitution</h3>
								<span class="subtitle">The Constitution of Nape</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">NAPE Anthem</h3>
								<span class="subtitle">The official anthem of our union</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Affiliations</h3>
								<span class="subtitle">Strength in Numbers</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 overview-col">
						<a class="item" href="#">
					
							<div class="hgroup">
								<h3 class="title">Quick Links</h3>
								<span class="subtitle">Helpful websites for NAPE Members</span>
							</div><!-- .hgroup -->
							
							<span class="button green">Read More</span>
					
						</a><!-- .item -->
					</div><!-- .col -->

					
				</div><!-- .grid -->
			
			</div><!-- .overview-blocks -->
			
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>