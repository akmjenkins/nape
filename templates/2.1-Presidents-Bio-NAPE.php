<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">President's Bio</h1>
					<span class="subtitle">Carol Furlong</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<div class="lazybg featured-img">
								<img src="../assets/dist/images/temp/featured-img.jpg" alt="nape president carol furlong">
							</div><!-- .lazybg -->
						
							<p>
								Carol was first elected president of the Newfoundland and Labrador Association of Public and Private Employees (NAPE) in June 2005, 
								the first woman to be elected to the position. She is also National Vice-president of the National Union of Public and General Employees (NUPGE).
							</p>
							 
							<p>
								Carol has been a union activist and a social activist most of her working career and has been involved in such issues as the fight for pay equity.
							</p>
							 
							<p>
								She has been involved in human rights and is outspoken on the women's rights internationally. She was an international election observer 
								and peace monitor in South Africa's first all race election in 1994 and was sent to the most volatile and dangerous area of South Africa in an 
								effort to help control the violence and ensure the election was free and fair.
							</p>
							 
							<p>
								She has baccalaureate degrees in Arts and in Education as well as a graduate degree in Education from Memorial University. She also holds a 
								Certificate in Teaching English as a Foreign Language and is a twice award winning author.
							</p>
							 
							<p>
								Since taking office, Carol has had a series of unprecedented successful victories on behalf of her  the Canadian Federation of Government 
								Employees Association, a National organization composed of eight other provincial government employee associations. This proved to be a 
								wise move because through the influence of the Canadian Federation, the provincial government was persuaded to set up a Civil Service 
								Liaison Committee, which was later replaced by a Joint Civil Service Council. The Joint Civil Service Council was the Association's first direct 
								channel to Government, which allowed the Association to present employment relations issues on a regular basis.
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>