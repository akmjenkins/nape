<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<div class="hgroup article-head">
				<h1 class="title">The Latest</h1>
				<span class="subtitle">Vivamus pulvinar tortor eget nibh gravida</span>
			</div><!-- .hgroup -->
				
			<div class="the-latest-header">
			
				<div class="news-update featured-news-update">
				
					<div class="news-update-head">
						<time datetime="2014-03-24" class="i blk">
							<span class="day">24</span> Mar
							<span class="year">2014</span>
						</time><!-- .i.blk -->
						
						<h3 class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
					</div><!-- .news-update-head -->
					
					<div class="news-update-content">
					
						<p>
							Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidu
							nt sapien ullamcorper at. Quisque mollis neque ultrices orci varius rhoncus.
							Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
							Fusce nec facilisis lorem, id posuere mi.
						</p>
					
					</div><!-- .news-update-content -->
					
					<div class="news-update-actions">
						<a href="#" class="button fill">More</a>
						<a href="#" class="button fill share">Share</a>
						<a href="#" class="button fill tweet">Tweet</a>
					</div><!-- .news-update-acions -->
				
				</div><!-- .news-update -->

				<div class="the-latest-header-image image">
					<div class="lazybg">
						<img src="../assets/dist/images/temp/news-big.jpg">
					</div><!-- .lazybg -->
				</div><!-- .the-latest-header-image -->
				
			</div><!-- .the-latest-header -->

		</div><!-- .sw -->
	</section>
	
	<section class="pad10">
		<div class="sw">
		
			<div class="grid eqh collapse-800 grid-tagged-items the-latest-grid">
				<div class="col col-3">
					<a class="item dark-bg grid-item-with-tag" href="#">
					
						<div class="the-latest-grid-img">
							<div class="lazybg" data-src="../assets/dist/images/temp/news.jpg"></div>
						</div><!-- .ar -->
					
						<div class="the-latest-grid-content">					
							<span class="item-tag tag-news-release">News Release</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">24</span> Mar
							</time>
							
							<p>
								NAPE Says Code Red Alert Increases Due to Shortage of Ambulances and Paramedic Staff.
							</p>
							
							<span class="button">Read More</span>
						</div><!-- .the-latest-grid-content -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<a class="item dark-bg grid-item-with-tag" href="#">
					
						<div class="the-latest-grid-img">
							<div class="lazybg" data-src="../assets/dist/images/temp/news-2.jpg"></div>
						</div><!-- .ar -->
				
						<div class="the-latest-grid-content">
							<span class="item-tag tag-alert">Alert</span>
							
							<time datetime="2014-03-03" class="i">
								<span class="day">03</span> Mar
							</time>
							
							<p>
								Eastern Health Paramedics to Hold Demonstration at Confederation Building Tomorrow.
							</p>
							
							<span class="button">Read More</span>
						</div><!-- .the-latest-grid-content -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<a class="item dark-bg grid-item-with-tag" href="#">
					
						<div class="the-latest-grid-img">
							<div class="lazybg" data-src="../assets/dist/images/temp/news-3.jpg"></div>
						</div><!-- .ar -->
					
						<div class="the-latest-grid-content">
							<span class="item-tag tag-event">Event</span>
							
							<time datetime="2014-03-12" class="i">
								<span class="day">12</span> Mar
							</time>
							
							<p>
								Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
							</p>
							
							
							<span class="button">Read More</span>
						</div><!-- .the-latest-grid-content -->
						
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<h3 class="section-title">Events</h3>
			<hr />
			
			<div class="grid collapse-950">
				<div class="col col-3-5">
					<div class="item">
					
						<div class="grid eqh collapse-650">
							<div class="col col-2">
								<a class="item grid-item-with-tag clear" href="#">
								
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">12</span> Feb
									</time>									
									
									<div class="hgroup">
										<h4 class="title">Seminar - Region 5</h4>
									</div><!-- .hgroup -->
									<div class="article-body">
										<p>
											A Shop Steward Seminar has been scheduled for 
											Region 5. This Seminar is meant for any 
											Shop Steward who has not received the 
											two-day training course.
										</p>
										
										<p>
											<strong>Date:</strong> <br />
											December 8 and 9, 2014 (Registration at 8:30 a.m.)
										</p>
										
										<p>
											<strong>Place:</strong> <br />
											Bay Roberts (Ground Search and Rescue)
										</p>
										
										<p>
											<strong>Time:</strong> <br />
											Monday, December 8 – 9:00 – 5:00 (Lunch will be provided) <br />
											Tuesday, December 9 – 9:00 – 5:00 (Lunch will be provided)
										</p>
									</div><!-- .article-body -->
									
									<span class="read-more-tag">Read More</span>
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<a class="item grid-item-with-tag clear" href="#">
								
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">15</span> Feb
									</time>									
									
									<div class="hgroup">
										<h4 class="title">Seminar - Region 5</h4>
									</div><!-- .hgroup -->
									<div class="article-body">
										<p>
											A Shop Steward Seminar has been scheduled for Region 5
										</p>
										
										<p>
											<strong>Date:</strong> <br />
											December 8 and 9, 2014 (Registration at 8:30 a.m.)
										</p>
										
										<p>
											<strong>Place:</strong> <br />
											Bay Roberts (Ground Search and Rescue)
										</p>
										
										<p>
											<strong>Time:</strong> <br />
											Monday, December 8 – 9:00 – 5:00 (Lunch will be provided) <br />
											Tuesday, December 9 – 9:00 – 5:00 (Lunch will be provided)
										</p>
									</div><!-- .article-body -->
									
									<span class="read-more-tag">Read More</span>
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2-5">
					<div class="item">
						
						<div class="media-meta">
							
							<div class="media-meta-item">
								<h4 class="title">President's Message</h4>		
								
								<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="20:13"></div>
								
							</div><!-- .media-meta-item -->
							
							<div class="media-meta-item">
								<h4 class="title">Gallery</h4>		
								
								<a href="#" class="lazybg gallery-link">
									<img src="../assets/dist/images/temp/news-big.jpg">
								</a><!-- .lazybg -->
								
							</div><!-- .media-meta-item -->
							
						</div><!-- .media-meta -->
						
					</div><!-- .item -->
				</div><!-- .col.col-2-5 -->
			</div><!-- .grid -->
			
			
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>