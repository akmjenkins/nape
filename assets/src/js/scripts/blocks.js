;(function(context) {

	var 
		debounce = context ? context.debounce : require('./debounce'),
		d = debounce(),
		$blocks,
		$window = $(window),
		methods = {
	
			getBlocks: function() {
				return $('.blocks').filter(function() { 
					var el = $(this);
					if(el.hasClass('always-list')) {
						el.addClass('list-view');
						return false;
					}
					return true;
				});
			},
	
			updateBlocks: function() {
			
				if(window.outerWidth < 650) {
					$blocks.addClass('list-view');
					return;
				}
				
				$blocks.each(function(i,blockGroup) {
					var collapseSize = blockGroup.className.match(/collapse-at-(\d+)/);
					if(collapseSize.length == 2 && window.outerWidth < (+collapseSize[1])) {
						$(blockGroup).addClass('list-view');
					} else {
						$(blockGroup).removeClass('list-view');
					}
				});
				
			},
	
		};

	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.blocks'); or just $(document).trigger('updateTemplate');
	$window.on('load resize updateTemplate.blocks',function(e) {
		if(!$blocks || e.type === 'updateTemplate') {
			$blocks = methods.getBlocks();
		}
		
		d.requestProcess(function() {
			methods.updateBlocks();
		});
		
	})
		//initial setup
		.trigger('resize');

}(typeof ns !== 'undefined' ? window[ns] : undefined));